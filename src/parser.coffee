class IcsParser
  VERSION = "0.0.1"

  constructor: (options = {}) ->

  parseDateString: (icalStr) =>
    [
      strYear
      strMonth
      strDay
      strHour
      strMin
      strSec
    ] = [
      icalStr.substr(0, 4)
      parseInt(icalStr.substr(4, 2), 10) - 1
      icalStr.substr(6, 2)
      icalStr.substr(9, 2)
      icalStr.substr(11, 2)
      icalStr.substr(13, 2)
    ]
    oDate = new Date(strYear, strMonth, strDay, strHour, strMin, strSec)
    oDate

  parseFile: (text) =>
    ics_data = text.split('\n')
    stack = []
    current_state = null
    calendar_object = null
    current_event = null
    for element in ics_data
      matched_object = element.trim().match(/([\w\-]+)\:([\w\W\d\a]+)$/)
      if matched_object?
        key = matched_object[1].toLowerCase()
        value = matched_object[2]
        switch
          when key == 'begin'
            switch value
              when 'VCALENDAR'
                calendar_object = new ICalendar()
                current_state = 'calendar'
              when 'VEVENT'
                current_event = new ICalendarEvent()
                current_state = 'event'
              else
                current_state = 'other'
            stack.push current_state
          when key == 'end'
            current_state = stack.pop()
            switch value
              when 'VEVENT'
                calendar_object.events.push current_event if calendar_object?
              when 'VCALENDAR'
                return calendar_object if stack.length == 0
          when key == 'version' and value? and calendar_object?
            calendar_object.version = value
          when key == 'calscale' and value? and calendar_object?
            calendar_object.calscale = value.toLowerCase()
          when key == 'prodid' and value? and calendar_object?
            calendar_object.prodid = value
          when key == 'x-apple-calendar-color' and value? and calendar_object?
            calendar_object.color = value
          when key == 'x-wr-calname' and value? and calendar_object?
            calendar_object.calname = value
          when key == 'dtend' and value? and current_event? and current_state == 'event'
            current_event.end = this.parseDateString(value)
          when key == 'dtstart' and value? and current_event? and current_state == 'event'
            current_event.start = this.parseDateString(value)
          when key == 'dtstamp' and value? and current_event? and current_state == 'event'
            current_event.stamp = this.parseDateString(value)
          when key == 'sequence' and value? and current_event? and current_state == 'event'
            current_event.sequence = parseInt(value)
          when key == 'uid' and value? and current_event? and current_state == 'event'
            current_event.uid = value.toLowerCase()
          when key == 'transp' and value? and current_event? and current_state == 'event'
            current_event.transp = value.toLowerCase()

    return

  @VERSION = VERSION

(exports ? this).IcsParser = IcsParser
