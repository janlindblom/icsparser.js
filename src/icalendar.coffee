class ICalendar

  constructor: (options = {}) ->
    @calscale = 'gregorian'
    { @version, @calscale, @prodid, @color, @calname } = options if options?
    @version = "2.0" if (options? and !options.version?) or !options?
    @calscale = "gregorian" if (options? and !options.calscale?) or !options?
    @events = []

(exports ? this).ICalendar = ICalendar

class ICalendarEvent

  constructor: (options = {}) ->
    { @end, @start, @stamp, @sequence, @transp, @uid } = options if options?

(exports ? this).ICalendarEvent = ICalendarEvent
