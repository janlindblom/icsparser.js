if typeof String::trim == 'undefined'
  # Implement String#trim unless already defined
  String::trim = ->
    String(this).replace /^\s+|\s+$/g, ''
