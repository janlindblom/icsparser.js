# icsparser.js

JavaScript ICS calendar parser.

[![Build Status](https://drone.io/bitbucket.org/janlindblom/icsparser.js/status.png)](https://drone.io/bitbucket.org/janlindblom/icsparser.js/latest)

## Usage

Load it in your `package.json`:

```json
"dependencies": {
  "icsparser.js": "~> 0.0.1"
}
```

Or in your `bower.json`:
```json
"dependencies": {
  "icsparser.js": "~0.0.1"
}
```

Then use it in your code:

```javascript
var icsp = require('dist/icsparser');
var icalObject = new icsp.ICalendar();
var parser = new icsp.IcsParser();
var dateObject = parser.parseDateString('20110914T184000Z');
```

## Copyright

ICSparser.js © 2015 by [Jan Lindblom](mailto:janlindblom@fastmail.fm).
ICSparser.js is licensed under the MIT license. Please see the
`LICENSE.txt` file for more information.

## Contributing

1. Fork it ( https://bitbucket.org/janlindblom/icsparser.js/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request