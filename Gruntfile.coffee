module.exports = (grunt) ->
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')
    banner: '/*!\n * <%= pkg.name %> - <%= pkg.description %>\n' +
            ' * @version v<%= pkg.version %>\n' +
            ' * @author <%= pkg.author.name %> \<<%= pkg.author.email %>\>\n' +
            ' * @link <%= pkg.homepage %>\n' +
            ' * @license <%= pkg.license %>\n' +
            ' * Copyright © <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>\n */',
    coffee:
      library:
        options:
          bare: true
          join: true
        files:
          'icsparser.js': [
            'src/extensions.coffee'
            'src/icalendar.coffee'
            'src/parser.coffee'
          ]
    jshint:
      all:
        options:
          expr: true
          eqnull: true
        files: 
          src: [ 'icsparser.js' ]
    mochaTest:
      test:
        options:
          reporter: 'spec'
          quiet: false
          require: 'coffee-script/register'
        src: [ 'spec/**/*.coffee' ]
    uglify:
      all:
        options:
          banner: '<%= banner %>'
          sourceMap: true
          mangle:
            except: [
              '$'
              'jQuery'
              'exports'
              'require'
              'trim'
              'VERSION'
              'ICalendar'
              'IcsParser'
              'ICalendarEvent'
            ]
        files:
          'icsparser.min.js': [
            'icsparser.js'
          ]
    usebanner:
      dist:
        options:
          position: 'top'
          banner: '<%= banner %>'
        files:
          src: ['icsparser.js']
    zip:
      dist:
        dest: 'pkg/<%= pkg.name %>-<%= pkg.version %>.zip'
        src: [
          'icsparser.js'
          'icsparser.min.js'
          'icsparser.min.js.map'
        ]

  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-jshint'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-banner'
  grunt.loadNpmTasks 'grunt-mocha-test'
  grunt.loadNpmTasks 'grunt-zip'

  grunt.registerTask 'build', [ 'coffee', 'usebanner', 'uglify' ]
  grunt.registerTask 'test', [ 'jshint', 'mochaTest' ]
  grunt.registerTask 'default', [ 'build', 'test' ]
  grunt.registerTask 'package', [ 'zip' ]
  return