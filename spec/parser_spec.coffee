fs = require('fs')
assert = require('assert')
expect = require('expect.js')
should = require('should')
icsp = require('../icsparser')

IcsParser = icsp.IcsParser
ICalendar = icsp.ICalendar

describe 'IcsParser', ->
  before ->
    @parser = new IcsParser()
    @ics_file = fs.readFileSync('test/freebusy.ics', 'utf8')

    return

  it 'has a proper version', ->
    should(IcsParser.VERSION).be.an.instanceOf(String)
    [v1, v2, v3] = (parseInt(name) for name in IcsParser.VERSION.split('.'))
    v1.should.be.an.instanceOf(Number)
    v2.should.be.an.instanceOf(Number)
    v3.should.be.an.instanceOf(Number)
    return

  it 'can parse ICS files', ->
    icalendar_object = @parser.parseFile @ics_file
    icalendar_object.should.have.property('version', '2.0')
    icalendar_object.should.have.property('calscale', 'gregorian')
    icalendar_object.should.have.property('prodid', '-//FastMail/1.0/EN')
    icalendar_object.should.have.property('color', '#44A703')
    icalendar_object.should.have.property('calname', 'Kalender')

    event1 = icalendar_object.events[0]
    should(event1.start).be.an.instanceOf(Date)
    event1.start.getFullYear().should.equal(2014)
    event1.start.getMonth().should.equal(9)
    event1.start.getDate().should.equal(27)
    event1.start.getHours().should.equal(8)
    event1.start.getMinutes().should.equal(45)
    event1.start.getSeconds().should.equal(0)
    should(event1.end).be.an.instanceOf(Date)
    should(event1.stamp).be.an.instanceOf(Date)
    should(event1.transp).equal('opaque')
    should(event1.sequence).equal(0)

    event2 = icalendar_object.events[1]
    should(event2.start).be.an.instanceOf(Date)
    event2.start.getFullYear().should.equal(2015)
    event2.start.getMonth().should.equal(9)
    event2.start.getDate().should.equal(24)
    event2.start.getHours().should.equal(7)
    event2.start.getMinutes().should.equal(0)
    event2.start.getSeconds().should.equal(0)
    should(event2.end).be.an.instanceOf(Date)
    should(event2.stamp).be.an.instanceOf(Date)
    should(event2.transp).equal('opaque')
    should(event2.sequence).equal(0)
    return

  it 'can parse iCal dates into Date objects', ->
    string = '20110914T184000Z'
    dateObj = @parser.parseDateString(string)
    dateObj.should.be.an.instanceOf(Date)
    dateObj.getFullYear().should.equal(2011)
    dateObj.getMonth().should.equal(8)
    dateObj.getDate().should.equal(14)
    dateObj.getHours().should.equal(18)
    dateObj.getMinutes().should.equal(40)
    dateObj.getSeconds().should.equal(0)
    return

  return