fs = require('fs')
assert = require('assert')
expect = require('expect.js')
should = require('should')
icsp = require('../icsparser')

describe 'ICalendar', ->
  describe 'represents an iCalendar feed', ->
    before ->
      @ics = new icsp.ICalendar()
      return

    it 'has a version property', ->
      @ics.should.have.property('version')
      return

    it 'has a calscale property', ->
      @ics.should.have.property('calscale')
      return

    it 'has a prodid property', ->
      @ics.should.have.property('prodid')
      return

    it 'has a color property', ->
      @ics.should.have.property('color')
      return

    it 'has a calname property', ->
      @ics.should.have.property('calname')
      return

    return

  describe 'default properties', ->
    before ->
      @ics = new icsp.ICalendar()
      return

    it 'has a default version', ->
      @ics.should.have.property('version', '2.0')
      return

    it 'has a default calscale', ->
      @ics.should.have.property('calscale', 'gregorian')
      return

    return

  return