/*!
 * icsparser.js - A JavaScript ICS file parser for the browser or node.
 * @version v0.0.1
 * @author Jan Lindblom <janlindblom@fastmail.fm>
 * @link https://bitbucket.org/janlindblom/icsparser.js
 * @license MIT
 * Copyright © 2015 Jan Lindblom
 */
var ICalendar, ICalendarEvent, IcsParser,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

if (typeof String.prototype.trim === 'undefined') {
  String.prototype.trim = function() {
    return String(this).replace(/^\s+|\s+$/g, '');
  };
}

ICalendar = (function() {
  function ICalendar(options) {
    if (options == null) {
      options = {};
    }
    this.calscale = 'gregorian';
    if (options != null) {
      this.version = options.version, this.calscale = options.calscale, this.prodid = options.prodid, this.color = options.color, this.calname = options.calname;
    }
    if (((options != null) && (options.version == null)) || (options == null)) {
      this.version = "2.0";
    }
    if (((options != null) && (options.calscale == null)) || (options == null)) {
      this.calscale = "gregorian";
    }
    this.events = [];
  }

  return ICalendar;

})();

(typeof exports !== "undefined" && exports !== null ? exports : this).ICalendar = ICalendar;

ICalendarEvent = (function() {
  function ICalendarEvent(options) {
    if (options == null) {
      options = {};
    }
    if (options != null) {
      this.end = options.end, this.start = options.start, this.stamp = options.stamp, this.sequence = options.sequence, this.transp = options.transp, this.uid = options.uid;
    }
  }

  return ICalendarEvent;

})();

(typeof exports !== "undefined" && exports !== null ? exports : this).ICalendarEvent = ICalendarEvent;

IcsParser = (function() {
  var VERSION;

  VERSION = "0.0.1";

  function IcsParser(options) {
    if (options == null) {
      options = {};
    }
    this.parseFile = bind(this.parseFile, this);
    this.parseDateString = bind(this.parseDateString, this);
  }

  IcsParser.prototype.parseDateString = function(icalStr) {
    var oDate, ref, strDay, strHour, strMin, strMonth, strSec, strYear;
    ref = [icalStr.substr(0, 4), parseInt(icalStr.substr(4, 2), 10) - 1, icalStr.substr(6, 2), icalStr.substr(9, 2), icalStr.substr(11, 2), icalStr.substr(13, 2)], strYear = ref[0], strMonth = ref[1], strDay = ref[2], strHour = ref[3], strMin = ref[4], strSec = ref[5];
    oDate = new Date(strYear, strMonth, strDay, strHour, strMin, strSec);
    return oDate;
  };

  IcsParser.prototype.parseFile = function(text) {
    var calendar_object, current_event, current_state, element, i, ics_data, key, len, matched_object, stack, value;
    ics_data = text.split('\n');
    stack = [];
    current_state = null;
    calendar_object = null;
    current_event = null;
    for (i = 0, len = ics_data.length; i < len; i++) {
      element = ics_data[i];
      matched_object = element.trim().match(/([\w\-]+)\:([\w\W\d\a]+)$/);
      if (matched_object != null) {
        key = matched_object[1].toLowerCase();
        value = matched_object[2];
        switch (false) {
          case key !== 'begin':
            switch (value) {
              case 'VCALENDAR':
                calendar_object = new ICalendar();
                current_state = 'calendar';
                break;
              case 'VEVENT':
                current_event = new ICalendarEvent();
                current_state = 'event';
                break;
              default:
                current_state = 'other';
            }
            stack.push(current_state);
            break;
          case key !== 'end':
            current_state = stack.pop();
            switch (value) {
              case 'VEVENT':
                if (calendar_object != null) {
                  calendar_object.events.push(current_event);
                }
                break;
              case 'VCALENDAR':
                if (stack.length === 0) {
                  return calendar_object;
                }
            }
            break;
          case !(key === 'version' && (value != null) && (calendar_object != null)):
            calendar_object.version = value;
            break;
          case !(key === 'calscale' && (value != null) && (calendar_object != null)):
            calendar_object.calscale = value.toLowerCase();
            break;
          case !(key === 'prodid' && (value != null) && (calendar_object != null)):
            calendar_object.prodid = value;
            break;
          case !(key === 'x-apple-calendar-color' && (value != null) && (calendar_object != null)):
            calendar_object.color = value;
            break;
          case !(key === 'x-wr-calname' && (value != null) && (calendar_object != null)):
            calendar_object.calname = value;
            break;
          case !(key === 'dtend' && (value != null) && (current_event != null) && current_state === 'event'):
            current_event.end = this.parseDateString(value);
            break;
          case !(key === 'dtstart' && (value != null) && (current_event != null) && current_state === 'event'):
            current_event.start = this.parseDateString(value);
            break;
          case !(key === 'dtstamp' && (value != null) && (current_event != null) && current_state === 'event'):
            current_event.stamp = this.parseDateString(value);
            break;
          case !(key === 'sequence' && (value != null) && (current_event != null) && current_state === 'event'):
            current_event.sequence = parseInt(value);
            break;
          case !(key === 'uid' && (value != null) && (current_event != null) && current_state === 'event'):
            current_event.uid = value.toLowerCase();
            break;
          case !(key === 'transp' && (value != null) && (current_event != null) && current_state === 'event'):
            current_event.transp = value.toLowerCase();
        }
      }
    }
  };

  IcsParser.VERSION = VERSION;

  return IcsParser;

})();

(typeof exports !== "undefined" && exports !== null ? exports : this).IcsParser = IcsParser;
